// g++ --std=c++11 -Wall -O0 test_01_*.cpp -o test

#ifndef __ARRAY2D__
#define __ARRAY2D__

// ******************************************************************************************************************
// Includes - I only used two!
// ******************************************************************************************************************
#include <iostream>
#include <stdexcept>

// ******************************************************************************************************************
// Class Definition
// ******************************************************************************************************************
template <typename T>
class array2d {
   private:
    // Member Variables
    T* data;
    size_t width;
    size_t height;

   public:
    // Construction / Destruction
    array2d<T>();   // Default
    array2d<T>(size_t width, size_t height);    // Width, height
    array2d<T>(size_t width, size_t height, T defVal);  // Width, height, default value
    ~array2d<T>();  // Deconstructor

    // Width / Height Accessors
    size_t get_width() const;
    size_t get_height() const;

    // 1D Index Operators
    T& operator[](const size_t index) const;
    T& operator[](const size_t index);

    // 2D Index Operators
    T& operator()(const size_t x, const size_t y) const;
    T& operator()(const size_t x, const size_t y);

    // Copy Constructor / Assignment Operator
    array2d<T>(const array2d& rhs); // Copy-constructor
    array2d<T>& operator=(const array2d& rhs);  // Copy-assignment

    // Move Constructor / Assignment Operator
    array2d<T>(array2d&& rhs);  // Move-constructor
    array2d<T>& operator=(array2d&& rhs);  // Move-assignment

    // Scalar Assignment Operator
    void operator=(T input);

    // Negation Operator
    array2d<T>& operator-();

    // Element-wise Operators
    array2d<T> operator*(const array2d& rhs) const; // Multiplication
    array2d<T> operator/(const array2d& rhs) const; // Division
    array2d<T> operator+(const array2d& rhs) const; // Addition
    array2d<T> operator-(const array2d& rhs) const; // Subtraction

    // Self Element-wise Operators
    array2d<T>& operator*=(const array2d& rhs); // Multiplication
    array2d<T>& operator/=(const array2d& rhs); // Division
    array2d<T>& operator+=(const array2d& rhs); // Addition
    array2d<T>& operator-=(const array2d& rhs); // Subtraction

    // Scalar Operators i.e. (x / 1)
    array2d<T> operator*(const T rhs) const; // Multiplication
    array2d<T> operator/(const T rhs) const; // Division
    array2d<T> operator+(const T rhs) const; // Addition
    array2d<T> operator-(const T rhs) const; // Subtraction

    // Self Scalar Operators i.e. (x /= 1) equiv to (x = x / 1)
    array2d<T>& operator*=(const T rhs); // Multiplication
    array2d<T>& operator/=(const T rhs); // Division
    array2d<T>& operator+=(const T rhs); // Addition
    array2d<T>& operator-=(const T rhs); // Subtraction

    // Friend Scalar Operators i.e. (1 / x) as opposed to (x / 1)
    template <typename U>
    friend array2d<U> operator*(U rhs, const array2d<U> lhs); // Multiplication
    template <typename U>
    friend array2d<U> operator/(U rhs, const array2d<U> lhs); // Division
    template <typename U>
    friend array2d<U> operator+(U rhs, const array2d<U> lhs); // Addition
    template <typename U>
    friend array2d<U> operator-(U rhs, const array2d<U> lhs); // Subtraction

    // Cast Operator
    template <typename U>
    operator array2d<U>() const;

    // std::ostream << Operator
    template <typename U>
    friend std::ostream& operator<<(std::ostream& os, const array2d<U>& rhs);
};

// ******************************************************************************************************************
// Construction / Destruction
// ******************************************************************************************************************

// Constructor: Default
template <typename T>
array2d<T>::array2d() : width(0), height(0) {}

// Constructor: Width and height
template <typename T>
array2d<T>::array2d(size_t _width, size_t _height) : width(_width), height(_height) {
    if (_width > 0 || _height > 0) {
        data = new T[width * height];  // Create a new array
    }
}

// Constructor: Width, height and default value
template <typename T>
array2d<T>::array2d(size_t _width, size_t _height, T _defVal) : width(_width), height(_height) {
    if (_width > 0 || _height > 0) {   // Check if the arrays are larger than 0
        data = new T[width * height];  // Create a new array

        // Fill array with default value
        for (size_t i = 0; i < (width * height); i++) {
            data[i] = _defVal;
        }
    }
}

// Deconstructor
template <typename T>
array2d<T>::~array2d() {
    if (width > 0 && height > 0) {
        width = 0;
        height = 0;
        delete[] data;
        data = nullptr;
    }
}

// ******************************************************************************************************************
// Width / Height Accessors
// ******************************************************************************************************************
template <typename T>
size_t array2d<T>::get_width() const {
    return width;
}

template <typename T>
size_t array2d<T>::get_height() const {
    return height;
}

// ******************************************************************************************************************
// 1D Index Operators
// ******************************************************************************************************************
template <typename T>
T& array2d<T>::operator[](const size_t index) const {
    return data[index];
}

template <typename T>
T& array2d<T>::operator[](const size_t index) {
    return data[index];
}

// ******************************************************************************************************************
// 2D Index Operators
// ******************************************************************************************************************
template <typename T>
T& array2d<T>::operator()(const size_t x, const size_t y) const {
    return data[(x * width + y)];
}

template <typename T>
T& array2d<T>::operator()(const size_t x, const size_t y) {
    return data[(x * width + y)];
}

// ******************************************************************************************************************
// Copy Constructor / Assignment Operator
// ******************************************************************************************************************

// Copy-constructor
template <typename T>
array2d<T>::array2d(const array2d& rhs) : width(rhs.get_width()), height(rhs.get_height()) {
    data = new T[(width * height)];  // Create a new array

    // Fill the array with the copied values
    for (size_t i = 0; i < (width * height); i++) {
        data[i] = rhs.data[i];
    }
}

// Copy-assignment
template <typename T>
array2d<T>& array2d<T>::operator=(const array2d<T>& rhs) {
    width = rhs.get_width();
    height = rhs.get_height();

    data = new T[(width * height)];  // Create a new array

    // Fill the 2d array with the copied values
    for (size_t i = 0; i < (width * height); i++) {
        data[i] = rhs.data[i];
    }

    return *this;
}

// ******************************************************************************************************************
// Move Constructor / Assignment Operator
// ******************************************************************************************************************

// Move constructor
template <typename T>
array2d<T>::array2d(array2d&& rhs) : width(rhs.get_width()), height(rhs.get_height()) {
    // Set rhs width and height to 0
    rhs.width = 0;
    rhs.height = 0;

    data = rhs.data;    // Move over data array
    delete [] rhs.data; // Deallocate rhs data array
    rhs.data = nullptr;
}

// Move assignment
template <typename T>
array2d<T>& array2d<T>::operator=(array2d&& rhs) {
    if (&rhs != this) {
        // Move over the values
        data = rhs.data;
        width = rhs.get_width();
        height = rhs.get_height();

        // Set rhs width and height to 0
        rhs.width = 0;
        rhs.height = 0;

        // Deallocate rhs data array
        delete [] rhs.data;
        rhs.data = nullptr;
    }

    return *this;
}

// ******************************************************************************************************************
// Scalar Assignment Operator
// ******************************************************************************************************************
template <typename T>
void array2d<T>::operator=(T input) {
    for (size_t i = 0; i < (width * height); i++) {
        data[i] = input;    // Set each of the array values to the input
    }
}

// ******************************************************************************************************************
// Negation Operator
// ******************************************************************************************************************
template <typename T>
array2d<T>& array2d<T>::operator-() {
    for (size_t i = 0; i < (width * height); i++) {
        data[i] = -data[i]; // Set each of the array values to its nagative
    }
    return *this;
}

// ******************************************************************************************************************
// Element-wise Operators
// ******************************************************************************************************************

/*
 *    Multiplication
 */
template <typename T>
array2d<T> array2d<T>::operator*(const array2d& rhs) const {
    if (width == rhs.get_width() && height == rhs.get_height()) {  // Check if the arrays are same size
        array2d<T> temp_data(width, height);

        for (size_t i = 0; i < (width * height); i++) {
            temp_data.data[i] = (data[i] * rhs.data[i]);  // Loop over data and perform operator
        }

        return temp_data;

    } else {  
        throw std::length_error("Array width or height doesnt match");  // Throw an error if arrays dont match
    }
}

/*
 *    Division
 */
template <typename T>
array2d<T> array2d<T>::operator/(const array2d& rhs) const {
    if (width == rhs.get_width() && height == rhs.get_height()) {  // Check if the arrays are same size
        array2d<T> temp_data(width, height);

        for (size_t i = 0; i < (width * height); i++) {
            temp_data.data[i] = (data[i] / rhs.data[i]);  // Loop over data and perform operator
        }

        return temp_data;

    } else {  
        throw std::length_error("Array width or height doesnt match");  // Throw an error if arrays dont match
    }
}

/*
 *    Addition
 */
template <typename T>
array2d<T> array2d<T>::operator+(const array2d& rhs) const {
    if (width == rhs.get_width() && height == rhs.get_height()) {  // Check if the arrays are same size
        array2d<T> temp_data(width, height);

        for (size_t i = 0; i < (width * height); i++) {
            temp_data.data[i] = (data[i] + rhs.data[i]);  // Loop over data and perform operator
        }

        return temp_data;

    } else {  
        throw std::length_error("Array width or height doesnt match");  // Throw an error if arrays dont match
    }
}

/*
 *    Subtraction
 */
template <typename T>
array2d<T> array2d<T>::operator-(const array2d& rhs) const {
    if (width == rhs.get_width() && height == rhs.get_height()) {  // Check if the arrays are same size
        array2d<T> temp_data(width, height);

        for (size_t i = 0; i < (width * height); i++) {
            temp_data.data[i] = (data[i] - rhs.data[i]);  // Loop over data and perform operator
        }

        return temp_data;

    } else {
        throw std::length_error(
            "Array width or height doesnt match");  // Throw an error if arrays dont match
    }
}

// ******************************************************************************************************************
// Self Element-wise Operators
// ******************************************************************************************************************

/*
 *    Self multiplication
 */
template <typename T>
array2d<T>& array2d<T>::operator*=(const array2d& rhs) {
    if (width == rhs.get_width() && height == rhs.get_height()) {  // Check if the arrays are same size
        for (size_t i = 0; i < (width * height); i++) {
            data[i] = (data[i] * rhs.data[i]);  // Loop over data and perform operator
        }

        return *this;

    } else {
        throw std::length_error(
            "Array width or height doesnt match");  // Throw an error if arrays dont match
    }

    
}

/*
 *    Self division
 */
template <typename T>
array2d<T>& array2d<T>::operator/=(const array2d& rhs) {
    if (width == rhs.get_width() && height == rhs.get_height()) {  // Check if the arrays are same size
        for (size_t i = 0; i < (width * height); i++) {
            data[i] = (data[i] / rhs.data[i]);  // Loop over data and perform operator
        }

        return *this;

    } else {
        throw std::length_error(
            "Array width or height doesnt match");  // Throw an error if arrays dont match
    }

    
}

/*
 *    Self addition
 */
template <typename T>
array2d<T>& array2d<T>::operator+=(const array2d& rhs) {
    if (width == rhs.get_width() && height == rhs.get_height()) {  // Check if the arrays are same size
        for (size_t i = 0; i < (width * height); i++) {
            data[i] = (data[i] + rhs.data[i]);  // Loop over data and perform operator
        }

        return *this;

    } else {
        throw std::length_error(
            "Array width or height doesnt match");  // Throw an error if arrays dont match
    }

    
}

/*
 *    Self subtraction
 */
template <typename T>
array2d<T>& array2d<T>::operator-=(const array2d& rhs) {
    if (width == rhs.get_width() && height == rhs.get_height()) {  // Check if the arrays are same size
        for (size_t i = 0; i < (width * height); i++) {
            data[i] = (data[i] - rhs.data[i]);  // Loop over data and perform operator
        }

        return *this;

    } else {
        throw std::length_error("Array width or height doesnt match");  // Throw an error if arrays dont match
    }

    
}

// ******************************************************************************************************************
// Scalar Operators
// ******************************************************************************************************************

/*
 *    Multiplication
 */
template <typename T>
array2d<T> array2d<T>::operator*(const T rhs) const{
    array2d<T> temp_data(width, height);    // Construct a new temporary array2d object

    for (size_t i = 0; i < (width * height); i++) {
        temp_data.data[i] = (data[i] * rhs);    // Loop over data and perform operator against rhs
    }

    return temp_data;
}

/*
 *    Division
 */
template <typename T>
array2d<T> array2d<T>::operator/(const T rhs) const {
    array2d<T> temp_data(width, height);    // Construct a new temporary array2d object

    for (size_t i = 0; i < (width * height); i++) {
        temp_data.data[i] = (data[i] / rhs);    // Loop over data and perform operator against rhs
    }

    return temp_data;
}

/*
 *    Addition
 */
template <typename T>
array2d<T> array2d<T>::operator+(const T rhs) const {
    array2d<T> temp_data(width, height);    // Construct a new temporary array2d object

    for (size_t i = 0; i < (width * height); i++) {
        temp_data.data[i] = (data[i] + rhs);    // Loop over data and perform operator against rhs
    }

    return temp_data;
}

/*
 *    Subtraction
 */
template <typename T>
array2d<T> array2d<T>::operator-(const T rhs) const {
    array2d<T> temp_data(width, height);    // Construct a new temporary array2d object

    for (size_t i = 0; i < (width * height); i++) {
        temp_data.data[i] = (data[i] - rhs);    // Loop over data and perform operator against rhs
    }

    return temp_data;
}

// ******************************************************************************************************************
// Self Scalar Operators
// ******************************************************************************************************************

/*
 *    Multiplication
 */
template <typename T>
array2d<T>& array2d<T>::operator*=(const T rhs) {
    for (size_t i = 0; i < (width * height); i++) {
        data[i] = (data[i] * rhs);    // Loop over data and perform operator against rhs
    }

    return *this;
}

/*
 *    Division
 */
template <typename T>
array2d<T>& array2d<T>::operator/=(const T rhs) {
    for (size_t i = 0; i < (width * height); i++) {
        data[i] = (data[i] / rhs);    // Loop over data and perform operator against rhs
    }

    return *this;
}

/*
 *    Addition
 */
template <typename T>
array2d<T>& array2d<T>::operator+=(const T rhs) {
    for (size_t i = 0; i < (width * height); i++) {
        data[i] = (data[i] + rhs);    // Loop over data and perform operator against rhs
    }

    return *this;
}

/*
 *    Subtraction
 */
template <typename T>
array2d<T>& array2d<T>::operator-=(const T rhs) {
    for (size_t i = 0; i < (width * height); i++) {
        data[i] = (data[i] - rhs);    // Loop over data and perform operator against rhs
    }

    return *this;
}

// ******************************************************************************************************************
// Friend Scalar Operators
// ******************************************************************************************************************

/*
 *    Multiplication
 */
template <typename U>
array2d<U> operator*(U rhs, const array2d<U> lhs) {
    array2d<U> temp_data(lhs.get_width(), lhs.get_height());    // Create a temporary array2d object from lhs values

    for (size_t i = 0; i < (lhs.get_width() * lhs.get_height()); i++) {
        temp_data.data[i] = (rhs * lhs.data[i]);        // Loop over data and perform operator against rhs
    }

    return temp_data;
}

/*
 *    Division
 */
template <typename U>
array2d<U> operator/(U rhs, const array2d<U> lhs) {
    array2d<U> temp_data(lhs.get_width(), lhs.get_height());    // Create a temporary array2d object from lhs values

    for (size_t i = 0; i < (lhs.get_width() * lhs.get_height()); i++) {
        temp_data.data[i] = (rhs / lhs.data[i]);        // Loop over data and perform operator against rhs
    }

    return temp_data;
}

/*
 *    Addition
 */
template <typename U>
array2d<U> operator+(U rhs, const array2d<U> lhs) {
    array2d<U> temp_data(lhs.get_width(), lhs.get_height());    // Create a temporary array2d object from lhs values

    for (size_t i = 0; i < (lhs.get_width() * lhs.get_height()); i++) {
        temp_data.data[i] = (rhs + lhs.data[i]);        // Loop over data and perform operator against rhs
    }

    return temp_data;
}

/*
 *    Subtraction
 */
template <typename U>
array2d<U> operator-(U rhs, const array2d<U> lhs) {
    array2d<U> temp_data(lhs.get_width(), lhs.get_height());    // Create a temporary array2d object from lhs values

    for (size_t i = 0; i < (lhs.get_width() * lhs.get_height()); i++) {
        temp_data.data[i] = (rhs - lhs.data[i]);        // Loop over data and perform operator against rhs
    }

    return temp_data;
}

// ******************************************************************************************************************
// Cast Operator
// ******************************************************************************************************************
template <typename T>
template <typename U>
array2d<T>::operator array2d<U>() const {
    array2d<U> temp_data(width, height);    // Create temporary array2d of template type U

    for (size_t i = 0; i < (width * height); i++) {
        temp_data[i] = (U) data[i];     // Loop over data and convert from template type T to U
    }

    return temp_data;
}

// ******************************************************************************************************************
// std::ostream << Operator
// ******************************************************************************************************************
template <typename U>
std::ostream& operator<<(std::ostream& os, const array2d<U>& rhs) {
    size_t count = 0;

    os << "[";  // Create first bracket

    for (size_t i = 0; i < (rhs.get_width() * rhs.get_height()); i++) {
        if (i == (rhs.get_width() * rhs.get_height()) - 1) {    // If its the end of the array, print ending brackets
            os << rhs.data[i] << " ]\t]";
            count++;

        } else if (count == 0) {    // If its the start, print first bracket (in the list)
            os << "\t[ " << rhs.data[i] << ", ";
            count++;

        } else if (count == (rhs.get_width() - 1)) {    // If its the end of the row, then end the line
            os << rhs.data[i] << " ]," << std::endl;
            count = 0;

        } else {    // Print an element on its own
            os << rhs.data[i] << ", ";
            count++;
        }
    }

    return os;  // Return the formatted array2d object
}

#endif